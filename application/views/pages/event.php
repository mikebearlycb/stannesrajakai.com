<div class="tm-slider-box tm-light-bg">
    <section class="tm-slider uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
        <!-- start full width -->
        <div class="uk-width-1-1">
            <div class="uk-panel top-pageslider CuteChurch">
                <div class="slider-module">
                    <div class="uk-slidenav-position" data-uk-slideshow="{height: &#039;300&#039;, animation: &#039;fade&#039;, duration: &#039;&#039;, autoplay: true, autoplayInterval: &#039;5000&#039;, videoautoplay: false, videomute: false, kenburns: false}">
                        <ul class="uk-slideshow uk-overlay-active">
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page.jpg" alt="bg_page" width="1920" height="360" class="aligncenter size-full" />
                            </li>
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page-01.jpg" alt="bg_page-01" width="1920" height="300" class="aligncenter size-full" />
                            </li>
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page-02.jpg" alt="bg_page-02" width="1920" height="300" class="aligncenter size-full" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end full width -->
    </section>
</div>

<div class="uk-container uk-container-center">
    <div class="uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-3-4 tm-middle">
            <main class="tm-content">






                <div id="tribe-events-pg-template">
                    <div id="tribe-events" class="tribe-no-js" data-live_ajax="0" data-datepicker_format="0" data-category="">
                        <div id="tribe-events-content" class="tribe-events-single vevent hentry">
                            



                            <!-- Notices -->
                            <h2 class="tribe-events-single-event-title summary entry-title">கோடி அற்புதர் புனித அந்தோணியார் ஆலய திருவிழா 2020</h2>
              <br/>
                            <div id="post-211" class="post-211 tribe_events type-tribe_events status-publish has-post-thumbnail tribe_events_cat-classes cat_classes">
                                <!-- Event featured image, but exclude link -->
                                <div class="tm-single-event-image">
                                    <div class="tribe-events-event-image"><img width="1024" height="300" src="<?=asset_url();?>/images/stantonys.png" class="attachment-event-image wp-post-image" alt="NEWS" />
                                    </div>
                                    <div class="tm-event-info">
                                        <div class="post-date">
                                            <div class="day">
                                                <span>16</span>
                                                <p class="">February</p>
                                                <p class=""></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Event meta -->
                                <div class="tribe-events-single-section tribe-events-event-meta primary tribe-clearfix">
                                    <div class="tribe-events-meta-group tribe-events-meta-group-details">
                                        <br/>
                                        <h3 class="tribe-events-single-section-title"> Details </h3>
                                        <dl>
                                            திருக்கொடியேற்றம் 16.02.2020 பெருவிழா 25.02.2020

                                        </dl>
                                    </div>
                                    
                                </div>
                                <!-- Event content -->
                                <div class="tribe-events-single-event-description tribe-events-content entry-content description">
                                   1-ம் திருவிழா 16.02.2020 ஞாயிறு மாலை 6 மணி: கொடியேற்றம் நேரலை (live telecast) செய்யப்படும்.
                                   <br><br>
                                    9-ம் திருவிழா 24.02.2020 திங்கள் காலை 6 மணி: திருப்பலி நேரலை செய்யப்படும். மாலை 6:30 மணி: மாலை ஆராதனை நேரலை செய்யப்படும்.
                                   <br><br>     
                                    10-ம் திருவிழா 25.02.2020 காலை 06:30 மணி: திருவிழா திருப்பலி நேரலை செய்யப்படும். மதியம் 12:00 மணிக்கு நடைபெறும் அசன நிகழ்ச்சியும் நேரலை செய்யப்படும்.
                                <br><br>
                                </div>
                               
                                <img width="1024" height="300" src="<?=asset_url();?>/images/stantonys1.jpeg" class="attachment-event-image wp-post-image" alt="NEWS" />
                                <br><br>

                                <img width="1024" height="300" src="<?=asset_url();?>/images/stantonys2.jpeg" class="attachment-event-image wp-post-image" alt="NEWS" />
  <br><br>
                                <!-- .tribe-events-cal-links -->
                            </div>
                         
                        </div>
                        <!-- #tribe-events-content -->
                    </div>
                    <!-- #tribe-events -->
                </div>
                <!-- #tribe-events-pg-template -->
















                <div id="tribe-events-pg-template">
                    <div id="tribe-events" class="tribe-no-js" data-live_ajax="0" data-datepicker_format="0" data-category="">
                        <div id="tribe-events-content" class="tribe-events-single vevent hentry">
                            



                            <!-- Notices -->
                            <h2 class="tribe-events-single-event-title summary entry-title">பரலோக அன்னை விழி திறந்த விழா</h2>
              <br/>
                            <div id="post-211" class="post-211 tribe_events type-tribe_events status-publish has-post-thumbnail tribe_events_cat-classes cat_classes">
                                <!-- Event featured image, but exclude link -->
                                <div class="tm-single-event-image">
                                    <div class="tribe-events-event-image"><img width="1024" height="300" src="<?=asset_url();?>/images/gall-img-006-1024x300.jpg" class="attachment-event-image wp-post-image" alt="NEWS" />
                                    </div>
                                    <div class="tm-event-info">
                                        <div class="post-date">
                                            <div class="day">
                                                <span>10</span>
                                                <p class="">November</p>
                                                <p class=""></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Event meta -->
                                <div class="tribe-events-single-section tribe-events-event-meta primary tribe-clearfix">
                                    <div class="tribe-events-meta-group tribe-events-meta-group-details">
                                        <br/>
                                        <h3 class="tribe-events-single-section-title"> Details </h3>
                                        <dl>
                                            பரலோக அன்னை விழி திறந்த விழாவை கொண்டாடும் இந்நன்னாளில் 
மதிய செப வழிபாடு (12:00 to 14:00 pm) மற்றும் தேர் பவனி
மாலை 7:00 pm ஆடம்பர கூட்டு திருப்பலி & விருந்து 
                                        </dl>
                                    </div>
                                    
                                </div>
                                <!-- Event content -->
                                <div class="tribe-events-single-event-description tribe-events-content entry-content description">
                                    அன்னை விழி திறந்த விழா

இரஜகிருஷ்ணாபுரம் தூய அன்னம்மாள் தேவாலயத்தில் பரலோக மாதா 2009 நவம்பர் மாதம் பத்தாம் தேதி அன்று மாலை 6.00 மணி அளவில் உறுதிபூசுதல் எடுப்பதற்கு படித்து கொண்டு இருந்த சிறுவர் சிறுமியர்களுக்கு தன்னுடைய கண்களை திறந்து காட்சி அளித்தார்கள். அதன் பிறகு 15 நாட்கள் தொடர்ந்து நிறைய மக்களுக்கு கண்களை திறந்து காட்சி அளித்தார்கள்.
               
                                </div>
                               
                                <!-- .tribe-events-cal-links -->
                            </div>
                         
                        </div>
                        <!-- #tribe-events-content -->
                    </div>
                    <!-- #tribe-events -->
                </div>
                <!-- #tribe-events-pg-template -->
            </main>
        </div>
     
    </div>
</div>