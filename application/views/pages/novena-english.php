<!--<div class="tm-default-header-offset-bg"></div> --> <!-- Start header offset-->

<div class="tm-slider-box tm-light-bg">
    <section class="tm-slider uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
        <!-- start full width -->
        <div class="uk-width-1-1">
            <div class="uk-panel top-pageslider CuteChurch">
                <div class="slider-module">
                    <div class="uk-slidenav-position" data-uk-slideshow="{height: &#039;300&#039;, animation: &#039;fade&#039;, duration: &#039;&#039;, autoplay: true, autoplayInterval: &#039;5000&#039;, videoautoplay: false, videomute: false, kenburns: false}">
                        <ul class="uk-slideshow uk-overlay-active">
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page.jpg" alt="bg_page" width="1920" height="360" class="aligncenter size-full" />
                            </li>
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page-01.jpg" alt="bg_page-01" width="1920" height="300" class="aligncenter size-full" />
                            </li>
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page-02.jpg" alt="bg_page-02" width="1920" height="300" class="aligncenter size-full" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end full width -->
    </section>
</div>



<div class="uk-container uk-container-center">
    <div class="uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1 tm-middle">
            <main class="tm-content">
                <section>
                    
                    <div id="primary" class="site-content post-content">
                        <div class="main-heading">
                            <h1 class="tm-page-title">Nine Day Novena to St. Ann</h1>
                        </div>
                        <div id="content" role="main">


<h3>Novena Prayers</h3>
<p>Glorious St. Anne, filled with compassion for those who invoke you, with love for those who suffer, heavily laden with the weight of my troubles, I knee at your feet and humbly beg you to take my present need under your special protection. (mention your request).</p>

<p>Vouchsafe to recommend it to your daughter, the Blessed Virgin Mary, and lay it before the throne of Jesus. Cease not to intercede for me until my request is granted. Above all, obtain for me the grace to one day meet God face to face, and with you and Mary and all the angels and saints praising Him through all eternity. Amen.</p>

<p>Our Father</p>
<p>Hail Mary</p>
<p>Glory be to the Father</p>
<p>Pray for us, Saint Anne. That we may be made worthy of the promises of Christ.</p>

<p>O Jesus, Holy Mary, St. Ann, help me now and at the hour of my death. Good St. Ann, intercede for me.</p>

<p>Now, pray the Prayers for the particular Day.</p>

<h3>Day 1</h3>

<p>Dear St. Ann, though I am but a prodigal child, I appeal to you and place myself under your great motherly care. Please listen to my prayers and grant my requests. See my contrite heart, and show me your unfailing goodness. Deign to be my advocate and recommend me to God’s infinite mercy. Obtain for me forgiveness of my sins and the strength to begin a new life that will last forever. Blessed St. Ann, I also beg of you the grace to love, to serve, and to honor your daughter, the most holy Virgin Mary. Please recommend me to her and pray to her for me. She refuses none of your requests but welcomes with loving kindness all those for whom you intercede. Good Jesus, be merciful to the faithful servants of Your grandmother St. Ann.</p>

<p>Great Saint Anne, engrave indelibly on my heart and in my mind the words that have reclaimed and sanctified so many sinners: “What shall it profit a man to gain the whole world if he lose his own soul?” May this be the principle fruit of these prayers by which I will strive to honor you during this novena. At your feet renew my resolution to invoke you daily, not only for the success of my temporal affairs and to be preserved from sickness and suffering, but above all, that I may be preserved from all sin and that I may succeed in working out my eternal salvation and that I will receive the special grace of (mention your request). O most powerful Saint Anne, do not let me lose my soul, but obtain for me the grace of winning my way to heaven, there with you, you blessed spouse, and your glorious daughter, to sing the praise of the most holy and adorable Trinity forever and ever. Amen.</p>

<h3>Day 2</h3>

<p>From the depths of my heart, good St. Ann, I offer you my homage this day and ask you to shelter me under the mantle of your motherly care. You know, good mother, how much I love you, how gladly I serve you, how happy I am to praise you, how eager I am to call on you in time of distress. Good St. Ann, be pleased to extend your helping hand in all my wants. Listen to my prayers, for I place my trust in your gracious bounty. Make all my thoughts and desires worthy and righteous. Jesus, I thank You for all the graces which in Your infinite goodness You have lavished upon St. Ann; for having chosen her, among all women, to be Your grandmother on earth and exalted her in heaven with such great and miraculous powers. In the name of her merits, I humbly recommend myself to Your infinite mercy.</p>

<p>Glorious Saint Anne, how can you be otherwise than overflowing with tenderness toward sinners like myself, since you are the grandmother of Him who shed His blood for them, and the mother of her whom the saints call advocate of sinners? To you, therefore, I address my prayers with confidence. Vouchsafe to commend me to Jesus and Mary so that, at your request, I may be granted remission of my sins, perseverance, the love of God, charity for all mankind, and the special grace of (mention your request), which I stand in need at the present time. O most powerful Protectress, let me not lose my soul, but obtain for me that through the merits of Jesus Christ and the intercession of Mary, I may have happiness of seeing them, of loving and praising them with your through all eternity. Amen.</p>

<h3>Day 3</h3>
<p>FHail, good St. Ann, who first responded to the needs of Mary, Mother of our Savior and Queen of Angels. Hail to you and to your husband St. Joachim, who watched over her infancy, presented her to the Lord in the temple and, according to your promise, consecrated her to the service of God. Hail St. Ann, good mother! I rejoice in the marvels you continually perform, because they encourage all to seek your intercession. Good St. Ann, by the great power that God has given you, show yourself my mother, my consoler, my advocate. Reconcile me to the God I have so deeply offended. Console me in my trials; strengthen me in my struggles. Deliver me from danger in my time of need. Help me at the hour of death and open to me the gates of paradise.</p>

Beloved of Jesus, Mary and Joseph, mother of the Queen of Heaven, take us and all who are dear to us under your special care. Obtain for us the virtues you instilled in the heart of her who was destined to become Mother of God, and the graces with which you were endowed. Sublime model of Christian womanhood, pray that we may imitate your example in our homes and families, listen to our petitions, and obtain our (mention your request). Guardian of the infancy and childhood of the most Blessed Virgin Mary, obtain the graces necessary for all who enter the marriage state, that imitating your virtues they may sanctify their homes and lead the souls entrusted to their care to eternal glory. Amen.</p>

<h3>Day 4</h3>
<p>FGood St. Ann, you offered your pure and holy daughter Mary in the temple with faith, piety and love. By the happiness which then filled your heart, I beg you to present me to your Grandson Jesus. Offered by you, I will be agreeable in His sight. Kind St. Ann, take me forever under your protection. Deliver me from the temptations which continually assail me. Above all, attend me in my last hour. As I lie on my deathbed, be present with your daughter to console and strengthen me.</p>
<p>Holy Mary and good St. Ann, show yourselves to be mothers indeed by obtaining for me the grace of a good death. When my soul goes forth, lead it to God’s tribunal so that, by your powerful help and intercession, it may obtain a favorable judgment.</p>

<p>FGlorious Saint Anne, I kneel in confidence at your feet, for you also have tasted the bitterness and sorrow of life. My necessities, the cause of my tears, are (mention your request). Good Saint Anne, you who did suffer much during the twenty years that precede your glorious maternity, I beseech you, by all your sufferings and humiliations, to grant my prayer. I pray to you, through your love for your glorious spouse Saint Joachim, through your love for your immaculate child, through the joy you did feel at the moment of her happy birth, not to refuse me. Bless me, bless my family and all who are dear to me, so that some day we may all be with you in the glory of heaven, for all eternity. Amen.</p>


 
<h3>Day 5</h3>
<p>FHail, all-powerful Lady. By God’s special favor, grant consolation to those who invoke you. Procure for them the eternal riches of heaven, and like a good mother, success in their temporal affairs as well. Good St. Ann, obtain my deliverance from the punishment which my sins deserve. Obtain for me success in my temporal affairs; especially see to the salvation of my soul. St. Ann, by your influence with Mary’s son Jesus, you have won the gift of conversion for many sinners. Will you then abandon me, who have chosen you as my mother? No, St. Ann. Your name alone, which signifies grace, assures me of the help of your prayers, and these prayers will surely procure pardon and mercy from Jesus. You will pray for me now and at the hour of my death.</p>

<p>FGreat Saint Anne, how far I am from resembling you. I so easily give way to impatience and discouragement; and so easily give up praying when God does not at once answer my request. Prayer is the key to all heavenly treasures and I cannot pray, because my weak faith and lack of confidence fail me at the slightest delay of divine mercy. O my powerful Protectress, come to my aid, listen to my petition (mention your request). Make my confidence and fervor, supported by the promise of Jesus Christ, redouble in proportion as the trial to which God in His goodness subjects me is prolonged, that I may obtain like you more than I can venture to ask for. In the future I will remember that I am made for heaven and not for earth; for eternity and not for time; that consequently I must ask, above all, the salvation of my soul which is assured to all who pray properly and who persevere in Pray. Amen.</p>

<h3>Day 6</h3>
<p>FGood St. Ann, do not allow my soul, a masterpiece of God’s creative power, to be lost forever. Free my heart of pride, vanity, self-love. May I know myself as I really am and learn meekness and simplicity of heart. God’s great love for me leaves me cold and unresponsive. I must reflect this love through works of mercy and charity toward my neighbor. In your boundless charity, good St. Ann, help me to merit the glorious crown which is given to those who have fought the good fight against the world, the devil and the flesh. Assist me to preserve purity of heart and body. With Mary and her divine Son, protect me always.</p>


 
<p>FGlorious Saint Anne, mother of the Mother of God, I beg you to obtain through your powerful intercession the pardon of my sins and the assistance I need in my troubles (mention your request). What can I not hope for if you deign to take me under your protection? The Most High has been please to grant the prayers of sinners, whenever you have been charitable enough to be their advocate. Kneeling at your feet, I beg you to help me in all spiritual and temporal dangers. To guide me in the true path of Christian perfection, and finally to obtain for me the grace of ending my life with the death of the just, so that I may contemplate face to face your beloved Jesus and daughter Mary in your loving companionship throughout eternity. Amen.</p>

<h3>Day 7</h3>
<p>FOnce again, Good St. Ann, I choose you for my advocate before the throne of God. By the power and grace that God has placed in you, extend to me your helping hand. Renew my mind and my heart. Dear St. Ann, I have unbounded confidence in your prayers. To your blessed hands I entrust my soul, my body and all my hopes for this world and the next. Direct my actions according to your goodness and wisdom. I place myself under your motherly care. Receive me, good mother. Cover me with the mantle of your love. Look kindly on me. By your powerful intercession, may I obtain from God grace and mercy. Obtain for me remission for sin and release from the punishment my offenses have deserved. Pray that I may receive grace to lead a devout life on earth and that I may obtain the everlasting reward of heaven.</p>

<p>FO Good Saint Anne, so justly called the mother of the infirm. The cure for those who suffer from disease, look kindly upon the sick for whom I pray. Alleviate their sufferings; cause them to sanctify their sufferings by patience and complete submission to the divine will; finally deign to obtain health for them and with it the firm resolution to honor Jesus, Mary, and yourself by the faithful performance of duties. But, merciful Saint Anne, I ask you above all for the salvation of my soul, rather than bodily health, for I am convinced that this fleeting life is given us solely to assure us a better one. Now, we cannot obtain that better life without the help of God’s graces. I earnestly beg them of you for the sick and for myself, especially the petition for which I am making in this novena (mention your request). Through the merits of our Lord Jesus Christ, through the intercession of His Immaculate Mother, and through the efficacious and powerful mediation, O glorious Saint Anne, Amen.</p>


 
<h3>Day 8</h3>
<p>FHail, St. Ann! I rejoice at your exalted glory. You gave birth to Mary, whose divine Son brought salvation to our lost world by conquering death and restoring life and hope to sinners. Pray to Him who, for love of us, clothed Himself with human flesh in the chaste womb of your daughter. Glorious St. Ann, with your blessed daughter, deliver me from everything that is displeasing in the sight of God. Pray to your gentle and powerful Grandson that He may cleanse my soul in His precious blood, that He may send His Holy Spirit to enlighten and direct me in all that I do, always obedient to His holy inspirations. Good mother, keep a watchful eye on me. Help me bear all my crosses. Give me the fullness of your bounty and sustain me with courage.</p>

<p>FRemember, O Saint Anne, you whose name signifies grace and mercy, that never was it known that anyone who fled to your protection, implored your help, and sought your intercession was left unaided. Inspired with this confidence, I fly unto you, good, and kind mother; I take refuge at your feet, burdened with the weight of my sins. O holy mother of the Immaculate Virgin Mary, despise not my petition (mention your request). But hear me and grant my prayer. Amen.</p>

<h3>Day 9</h3>
<p>FGood St. Ann, I have reached the end of this novena in your honor. I have asked and ask again. Good mother, let not your kind ear grow weary of my prayers, though I repeat them so often. Bounteous Lady, implore for me from divine Providence all the help I need through life. May your generous hand bestow on me the material means to satisfy my own needs and to alleviate the plight of the poor. Good St. Ann, fortify me by the sacraments of the Church at the hour of my death. Admit me into the company of the blessed in the kingdom of heaven, where I may praise and thank the adorable Trinity, your grandson Christ Jesus, your glorious daughter Mary, and yourself, dear St. Ann, through endless ages.</p>

<p>FMost holy mother of the Virgin Mary, glorious Saint Anne, I, a miserable sinner, confiding in your kindness, choose you today as my special advocate. I offer and consecrate my person and all my interests to your care and maternal solicitude. I hope o server you and honor you all my life for the love of your most holy daughter and to do all in my power to spread devotion to you. O my very good mother and advocate, deign to accept me as your servant, and to adopt me as you child. O glorious Saint Anne, I beg you, by the passion of my most loving Jesus, the Son of Mary, your most holy daughter, to assist me in all the necessities both of my body and my soul. Venerable Mother, I beg you to obtain for me the favor I seek in this novena, (mention your request) and the grace of leading a life perfectly conformable in all things to the divine will. I place my soul in your hands and in those of your kind daughter. I confide it to you, above all at the moment when it will be about to separate itself from my body in order that, appearing under your patronage before the Supreme Judge, He may find it worthy of enjoying His Divine Presence in your holy companionship in Heaven. Amen.</p>

                       </pre>
                    </div>
                        <!-- #content -->
                    </div>
                    <!-- #primary -->
                </section>
            </main>
        </div>
    </div>
</div>
