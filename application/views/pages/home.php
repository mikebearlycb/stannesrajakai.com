


<div class="tm-slider-box tm-dark-bg">
	<section class="tm-slider uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
	<!-- start full width -->
		<div class="uk-width-1-1">
			<div class="uk-panel top-slider-width CuteChurch">
	            <div class="slider-module">
	            	<div class="uk-slidenav-position" data-uk-slideshow="{height: 'auto', animation: 'fade', duration: '', autoplay: false, autoplayInterval: '6000', videoautoplay: false, videomute: false, kenburns: false}">
	                	<ul class="uk-slideshow uk-overlay-active">
	                        <li aria-hidden="false" class="uk-cover uk-height-viewport  tm-wrap uk-active">
	                        	<div class="uk-cover-background uk-position-cover"></div>
	                        	<img src="<?=asset_url();?>images/slider-img-07.jpeg" alt="slider-img-04" class="aligncenter size-full wp-image-631" height="960" width="1920">
								<div class="uk-overlay-panel uk-overlay-right uk-overlay-slide-left uk-flex uk-flex-center uk-flex-middle uk-width-1-1">
									<div class="uk-container uk-container-center tm-full-width">
										<div class=" tm-wrap">
											<div class="tm-slide-style-1 uk-width-1-1">
												<div class="">
													<div class="imageslider-background"><h3 class="slide-head">புனித அன்னம்மாள் ஆலயம், இரஜகை</h3></div><br><br>
													<!--<div style="padding: 10px;" class="imageslider-background">-->

                                                    <!--இன்று November 10, 2019 பரலோக அன்னை விழி திறந்த விழாவை கொண்டாடும் இந்நன்னாளில் மதிய செப வழிபாடு (12:00 to 14:00 pm) மற்றும் தேர் பவனி மாலை 7:00 pm ஆடம்பர கூட்டு திருப்பலி & விருந்து -->
                                                    

                                                    <!--நிகழ்ச்சிகளை நேரலையில் நமது ஊர் websiteல் காண ஏற்பாடு செய்யப்பட்டுள்ளது.  -->
                                                    <!--<br/>
                                                    <a href="/index.php/sundaymass">குருத்து ஞாயிறு பதிவு </a>
                                                    <br/>-->
                                                    <!--</div>-->

												<!--</div>-->
                                                
												<a href="#" class="tm-imageslider-button color-dark">இனிய திருநாள் நல்வாழ்த்துக்கள் </a>
                                                <style>

.rajakai_live {
  background-color: #004A7F;
  -webkit-border-radius: 10px;
  border-radius: 10px;
  border: none;
  color: #FFFFFF;
  cursor: pointer;
  display: inline-block;
  font-family: Arial;
  font-size: 20px;
  padding: 5px 10px;
  text-align: center;
  text-decoration: none;
  -webkit-animation: glowing 1500ms infinite;
  -moz-animation: glowing 1500ms infinite;
  -o-animation: glowing 1500ms infinite;
  animation: glowing 1500ms infinite;
}
@-webkit-keyframes glowing {
  0% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; -webkit-box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
}

@-moz-keyframes glowing {
  0% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; -moz-box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
}

@-o-keyframes glowing {
  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
}

@keyframes glowing {
  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
}
                                                </style>
                                                <br/><br/>
                                                <a class="rajakai_live" href="/index.php/livetv">Rajakai Live</a>
											</div>
										</div>
									</div>
								</div>
							</li>
	                        <li aria-hidden="true" class="uk-cover uk-height-viewport tm-wrap">
	                        	<div class="uk-cover-background uk-position-cover"></div>
	                        	<img src="<?=asset_url();?>images/slider-img-06.jpeg" alt="slider-img-06" class="aligncenter size-full wp-image-630" height="960" width="1920">
								<div class="uk-overlay-panel uk-overlay-right uk-overlay-slide-right uk-flex uk-flex-center uk-flex-middle uk-width-1-1">
									<div class="uk-container uk-container-center tm-full-width">
										<div class=" tm-wrap">
											<div class="tm-slide-style-2 uk-width-1-1">
												<div class="">
													<div class="imageslider-background"><h3 class="slide-head">St Annes Church, Rajakai</h3></div><br>
													<div class="imageslider-background"><h3 class="slide-head">Welcome</h3></div>
												</div>
												<a href="/" class="tm-imageslider-button color-light">More&nbsp;<i class="uk-icon-long-arrow-right"></i></a>
											</div>
										</div>
									</div>
								</div>
							</li>
	                        <li aria-hidden="true" class="uk-cover uk-height-viewport tm-wrap">
	                        	<div class="uk-cover-background uk-position-cover"></div>
	                        	<img src="<?=asset_url();?>images/slider-img-01.jpeg" alt="slider-img-01" class="aligncenter size-full wp-image-629" height="960" width="1920">
								<div class="uk-overlay-panel uk-overlay-right uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-width-1-1">
									<div class="uk-container uk-container-center tm-full-width">
										<div class=" tm-wrap">
											<div class="tm-slide-style-1 uk-width-7-10">
												<div class="uk-overlay-background tm-p-10">
													<div class="slide-text-primary tm-heading-font">St Annes Church, Rajakai</div>
													<h3 class="slide-head">Welcome</h3>
												</div>
												<a href="/" class="tm-imageslider-button color-light">More&nbsp;<i class="uk-icon-long-arrow-right"></i></a>
											</div>
										</div>
									</div>
								</div>
							</li>
	                		<li aria-hidden="true" class="uk-cover uk-height-viewport tm-wrap">
	                			<div class="uk-cover-background uk-position-cover"></div>
	                			<img src="<?=asset_url();?>images/slider-img-04.jpeg" alt="slider-img-05" class="aligncenter size-full wp-image-675" height="960" width="1920">
								<div class="uk-overlay-panel uk-overlay-right uk-overlay-fade uk-flex uk-flex-center uk-flex-middle uk-width-1-1">
									<div class="uk-container uk-container-center tm-full-width">
										<div class=" tm-wrap">
											<div class="tm-slide-style-2 uk-width-7-10">
												<div class="uk-overlay-background tm-p-10">
													<h3 class="slide-head">Martin Luther</h3>
													<div class="slide-text-primary tm-heading-font">"To gather with God's people in united adoration of the Father is as necessary to the Christian life as prayer."</div>
												</div>
												<a href="sermons.html" class="tm-imageslider-button color-light">More&nbsp;<i class="uk-icon-long-arrow-right"></i></a>
											</div>
										</div>
									</div>
								</div>
							</li>
	                        <li aria-hidden="true" class="uk-cover uk-height-viewport tm-wrap">
	                        	<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAeAAAAEOCAYAAABRmsRnAAACDUlEQVR4nO3BMQEAAADCoPVPbQo/oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgZep3AAEMQQcfAAAAAElFTkSuQmCC" alt='' style="width:100%;height:auto;">
	                        	<iframe data-uk-cover="{}" style="pointer-events: none; width: 1583px; height: 891px;" class="uk-position-absolute" data-player-id="sw-1" src="https://www.youtube.com/embed/slNcqdxdiVc" allowfullscreen="" height="270" width="480"></iframe>
							</li>
	                    </ul>
	                    <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
	                    <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
	                    <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
	                        <li class="uk-active" data-uk-slideshow-item="0"><a href="">0</a></li>
	                        <li data-uk-slideshow-item="1"><a href="">1</a></li>
	                        <li data-uk-slideshow-item="2"><a href="">2</a></li>
	                        <li data-uk-slideshow-item="3"><a href="">3</a></li>
	                        <li data-uk-slideshow-item="4"><a href="">4</a></li>
	                	</ul>
	                </div>
	        	</div>
	    	</div>
		</div>
		<!-- end full width -->
	</section>
</div>


<div class="tm-top-a-box tm-dark-pattern-bg">
    <div class="uk-container uk-container-center">
        <section class="tm-top-a uk-grid tm-xlarge-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <div class="uk-panel CountDownTimer">
                    <div id="countdowntimer-2-countdown" class="tminus_countdown" style="width:auto; height:auto;">
                        <div class="cutechurch-countdown omitweeks">
                            <div id="countdowntimer-2-tophtml" class="cutechurch-tophtml">
                                <h1 class="tm-title tcenter">
                                    <span class="border">வருங்கால நிகழ்வு</span>
                                </h1>
                                <p>புனித அன்னம்மாள் ஆலய திருவிழா 2020</p>
                                <h4>
                                    <span class="brackets">
                                        <a href="/" class="tm-color-line">17 July 2021 to 26 July 2021</a>
                                    </span>
                                </h4>
                            </div>
														<script>
// Set the date we're counting down to
var d = new Date();

var y = d.getFullYear()+1;
var m = d.getMonth();
var ya = d.getFullYear();
var da = d.getDate();

if (m == 6 && da < 17) 
 var countDownDate = new Date("Jul 17, "+ya.toString()+" 00:00:00").getTime();
else if (m == 6 && da >= 17) 
 var countDownDate = new Date("Jul 17, "+y.toString()+" 00:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
if (m == 6 && da >= 17 && da <= 27) {
    document.getElementById("days").innerHTML = 0;
    document.getElementById("hours").innerHTML = 0;
    document.getElementById("minutes").innerHTML = 0;
    document.getElementById("seconds").innerHTML = 0; 
} 
else {
	document.getElementById("days").innerHTML = days;
	document.getElementById("hours").innerHTML = hours;
	document.getElementById("minutes").innerHTML = minutes;
	document.getElementById("seconds").innerHTML = seconds;
}


  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    //document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);

// helper functions
const PI2 = Math.PI * 2
const random = (min, max) => Math.random() * (max - min + 1) + min | 0
const timestamp = _ => new Date().getTime()

// container
class Birthday {
  constructor() {
    this.resize()

    // create a lovely place to store the firework
    this.fireworks = []
    this.counter = 0

  }
  
  resize() {
    this.width = canvas.width = window.innerWidth
    let center = this.width / 2 | 0
    this.spawnA = center - center / 4 | 0
    this.spawnB = center + center / 4 | 0
    
    this.height = canvas.height = window.innerHeight
    this.spawnC = this.height * .1
    this.spawnD = this.height * .5
    
  }
  
  onClick(evt) {
     let x = evt.clientX || evt.touches && evt.touches[0].pageX
     let y = evt.clientY || evt.touches && evt.touches[0].pageY
     
     let count = random(3,5)
     for(let i = 0; i < count; i++) this.fireworks.push(new Firework(
        random(this.spawnA, this.spawnB),
        this.height,
        x,
        y,
        random(0, 260),
        random(30, 110)))
          
     this.counter = -1
     
  }
  
  update(delta) {
    ctx.globalCompositeOperation = 'hard-light'
    ctx.fillStyle = `rgba(20,20,20,${ 7 * delta })`
    ctx.fillRect(0, 0, this.width, this.height)

    ctx.globalCompositeOperation = 'lighter'
    for (let firework of this.fireworks) firework.update(delta)

    // if enough time passed... create new new firework
    this.counter += delta * 3 // each second
    if (this.counter >= 1) {
      this.fireworks.push(new Firework(
        random(this.spawnA, this.spawnB),
        this.height,
        random(0, this.width),
        random(this.spawnC, this.spawnD),
        random(0, 360),
        random(30, 110)))
      this.counter = 0
    }

    // remove the dead fireworks
    if (this.fireworks.length > 1000) this.fireworks = this.fireworks.filter(firework => !firework.dead)

  }
}

class Firework {
  constructor(x, y, targetX, targetY, shade, offsprings) {
    this.dead = false
    this.offsprings = offsprings

    this.x = x
    this.y = y
    this.targetX = targetX
    this.targetY = targetY

    this.shade = shade
    this.history = []
  }
  update(delta) {
    if (this.dead) return

    let xDiff = this.targetX - this.x
    let yDiff = this.targetY - this.y
    if (Math.abs(xDiff) > 3 || Math.abs(yDiff) > 3) { // is still moving
      this.x += xDiff * 2 * delta
      this.y += yDiff * 2 * delta

      this.history.push({
        x: this.x,
        y: this.y
      })

      if (this.history.length > 20) this.history.shift()

    } else {
      if (this.offsprings && !this.madeChilds) {
        
        let babies = this.offsprings / 2
        for (let i = 0; i < babies; i++) {
          let targetX = this.x + this.offsprings * Math.cos(PI2 * i / babies) | 0
          let targetY = this.y + this.offsprings * Math.sin(PI2 * i / babies) | 0

          birthday.fireworks.push(new Firework(this.x, this.y, targetX, targetY, this.shade, 0))

        }

      }
      this.madeChilds = true
      this.history.shift()
    }
    
    if (this.history.length === 0) this.dead = true
    else if (this.offsprings) { 
        for (let i = 0; this.history.length > i; i++) {
          let point = this.history[i]
          ctx.beginPath()
          ctx.fillStyle = 'hsl(' + this.shade + ',100%,' + i + '%)'
          ctx.arc(point.x, point.y, 1, 0, PI2, false)
          ctx.fill()
        } 
      } else {
      ctx.beginPath()
      ctx.fillStyle = 'hsl(' + this.shade + ',100%,50%)'
      ctx.arc(this.x, this.y, 1, 0, PI2, false)
      ctx.fill()
    }

  }
}

let canvas = document.getElementById('birthday')
let ctx = canvas.getContext('2d')

let then = timestamp()

let birthday = new Birthday
window.onresize = () => birthday.resize()
document.onclick = evt => birthday.onClick(evt)
document.ontouchstart = evt => birthday.onClick(evt)

  ;(function loop(){
    requestAnimationFrame(loop)

    let now = timestamp()
    let delta = now - then

    then = now
    birthday.update(delta / 1000)
    

  })()
</script>

                            <div id="countdowntimer-2-dashboard" class="cutechurch-dashboard">
                                <div class="cutechurch-dash cutechurch-days_dash">
                                    <div class="cutechurch-dash_title">days</div>
                                    <div id="days" class="cutechurch-digit"></div>
                                </div>
                                <div class="cutechurch-dash cutechurch-hours_dash">
                                    <div class="cutechurch-dash_title">hours</div>
                                    <div id="hours" class="cutechurch-digit"></div>
                                </div>
                                <div class="cutechurch-dash cutechurch-minutes_dash">
                                    <div class="cutechurch-dash_title">minutes</div>
                                    <div id="minutes" class="cutechurch-digit"></div>
                                </div>
                                <div class="cutechurch-dash cutechurch-seconds_dash">
                                    <div class="cutechurch-dash_title">seconds</div>
                                    <div id="seconds" class="cutechurch-digit"></div>
                                </div>
                            </div>

                            <div id="countdowntimer-2-bothtml" class="cutechurch-bothtml"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


<!--
<div class="tm-top-b-box tm-light-bg">
    <div class="uk-container uk-container-center">
        <section class="tm-top-b uk-grid tm-xlarge-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <div class="uk-panel CuteEvents">
                    <div class="tm-widget-top-text">
                        <h1 class="tm-title tcenter">
                            <span class="border">Events</span>
                        </h1>
						In sollicitudin, ipsum nec auctor malesuada, elit turpis fermentum odio, ac vestibulum dui risus non elit. Quisque varius ex arcu, vitae rhoncus justo lacinia ac. Nunc eu bibendum mauris, et tincidunt ex. Duis non aliquam nibh.
                    </div>
                    <div class="uk-grid uk-grid-collapse">
                        <ul class="tm-category-posts-block">
                            <li class="uk-width-small-1-1 uk-width-medium-1-2 tribe-events-list-widget-events hentry vevent type-tribe_events post-857 tribe-clearfix">
                                <div class="tribe-events-event-image">
                                	<a href="event.html">
                                    	<img src="<?=asset_url();?>images/gall-img-011-198x330.jpg" class="attachment-thumbnail wp-post-image" alt="gall-img-011" height="330" width="198">
                                    </a>
                                </div>
                                <div class="post-date">
                                    <div class="day">
                                        <span>12</span>
                                        <p>May</p>
                                    </div>
                                    <div class="time">
                                        <p>8:30 am</p>
                                        <p class="tm-event-venue">Church of Jesus Christ</p>
                                    </div>
                                </div>
                                <div style="padding: 20px;">
                                    <h3 class="uk-panel-header">
                                        <a href="event.html">I am going to send you what</a>
                                    </h3>
                            		In sollicitudin, ipsum nec auctor malesuada, elit turpis fermentum odio, ac vestibulum dui risus non…
                                    <a href="event.html" class="read-more" rel="bookmark">Read more</a>
                                </div>
                            </li>
                            <li class="uk-width-small-1-1 uk-width-medium-1-2 tribe-events-list-widget-events hentry vevent type-tribe_events post-857 tribe-clearfix">
                                <div class="tribe-events-event-image">
                                    <a href="event.html">
                                        <img src="<?=asset_url();?>images/gall-img-0031-198x330.jpg" class="attachment-thumbnail wp-post-image" alt="NEWS" height="330" width="198">
                                    </a>
                                </div>
                                <div class="post-date">
                                    <div class="day">
                                        <span>15</span>
                                        <p>Jun</p>
                                    </div>
                                    <div class="time">
                                        <p>8:00 am</p>
                                        <p class="tm-event-venue">Bethlehem Church</p>
                                    </div>
                                </div>
                                <div style="padding: 20px;">
                                    <h3 class="uk-panel-header">
                                        <a href="event.html">Child Dedications</a>
                                    </h3>
                       				 Etiam risus mauris, fermentum sit amet metus ut, vulputate ullamcorper est. Aenean ac turpis dui.…
                                    <a href="event.html" class="read-more" rel="bookmark">Read more</a>
                                </div>
                            </li>
                            <li class="uk-width-small-1-1 uk-width-medium-1-2 tribe-events-list-widget-events hentry vevent type-tribe_events post-857 tribe-clearfix">
                                <div class="tribe-events-event-image">
                                    <a href="event.html">
                                        <img src="<?=asset_url();?>images/gall-img-006-198x330.jpg" class="attachment-thumbnail wp-post-image" alt="NEWS" height="330" width="198">
                                    </a>
                                </div>
                                <div class="post-date">
                                    <div class="day">
                                        <span>10</span>
                                        <p>Mar</p>
                                    </div>
                                    <div class="time">
                                        <p>8:00 am</p>
                                        <p class="tm-event-venue">Bethlehem Church</p>
                                    </div>
                                </div>
                                <div style="padding: 20px;">
                                    <h3 class="uk-panel-header">
                                        <a href="event.html">Art lessons for Kids</a>
                                    </h3>
                    				Nullam at varius nulla. Proin ut scelerisque ipsum. Aliquam in mattis leo. In fringilla accumsan…
                                    <a href="event.html" class="read-more" rel="bookmark">Read more</a>
                                </div>
                            </li>
                            <li class="uk-width-small-1-1 uk-width-medium-1-2 tribe-events-list-widget-events hentry vevent type-tribe_events post-857 tribe-clearfix">
                                <div class="tribe-events-event-image">
                                    <a href="event.html">
                                        <img src="<?=asset_url();?>images/gall-img-0041-198x330.jpg" class="attachment-thumbnail wp-post-image" alt="EVENTS" height="330" width="198">
                                    </a>
                                </div>
                                <div class="post-date">
                                    <div class="day">
                                        <span>14</span>
                                        <p>Jun</p>
                                    </div>
                                    <div class="time">
                                        <p>8:00 am</p>
                                        <p class="tm-event-venue">Bethlehem Church</p>
                                    </div>
                                </div>
                                <div style="padding: 20px;">
                                    <h3 class="uk-panel-header">
                                        <a href="event.html">Holy shift: Strategic think</a>
                                    </h3>
                					Praesent lobortis, tellus vel placerat iaculis, neque leo tempus nisi, vitae gravida mi elit gravida…
                                    <a href="event.html" class="read-more" rel="bookmark">Read more</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                	<a href="event-list.html" rel="bookmark" class="tm-event-widget-button">All Events</a>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="tm-top-c-box tm-dark-bg">
    <div class="uk-container uk-container-center">
        <section class="uk-grid-collapse tm-top-c uk-grid tm-xlarge-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <div class="uk-panel widget_text">
                    <img class="tm-img-center" src="<?=asset_url();?>images/image-002.jpg" alt="image-002.jpg">
                    <h1 class="tm-title tm-text-center tm-p-top-medium">
                        <span class="border">About</span>
                    </h1>
                    <div class="tm-text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae lectus urna. Praesent ut sodales quam. Curabitur lobortis tincidunt facilisis. Suspendisse et malesuada justo, et consequat sapien. Etiam at consectetur augue. Donec id orci imperdiet, tincidunt lectus at, commodo lacus. Aliquam erat volutpat. Proin eu viverra leo. Aliquam rutrum tincidunt nibh vitae sollicitudin. Donec in purus eu sem pellentesque tincidunt. Sed ornare ante id ullamcorper suscipit. Phasellus purus dui, maximus ac sem sodales, pretium faucibus magna.</div>
                    <a href="about-us.html" title="" class="tm-button-center">Read more</a>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="tm-top-d-box tm-dark-bg">
    <section class="uk-grid-collapse tm-top-d uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <div class="uk-width-1-1 uk-width-large-1-2">
            <div style="min-height: 210px;" class="uk-panel CuteBibleText">
                <div style="" data-uk-slideset="{default: 1}" class="tm-bible-slideset">
                    <div class="slider-bible">
                        <ul class="uk-grid uk-slideset uk-grid-width-1-1">
                            <li style="" class="uk-push-2-5 uk-grid-width-small-1-1 uk-width-medium-3-5  uk-width-large-3-5 uk-active">
                                <div class="uk-text-center text-shadow">They all ate and were satisfied. And they picked up twelve wicker baskets full of fragments and what was left…</div>
                                <div class="tm-text-center">
                                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                    <h3 class="uk-panel-header uk-text-center">
                                        <a href="#">Mk 6:42-43</a>
                                    </h3>
                                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                </div>
                            </li>
                            <li style="display: none;" class="uk-push-2-5 uk-grid-width-small-1-1 uk-width-medium-3-5  uk-width-large-3-5">
                                <div class="uk-text-center text-shadow">Then, taking the five loaves and the two fish and looking up to heaven, he said the blessing, broke the…</div>
                                <div class="tm-text-center">
                                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                    <h3 class="uk-panel-header uk-text-center">
                                        <a href="#">Mark 6:41a</a>
                                    </h3>
                                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                </div>
                            </li>
                            <li style="display: none;" class="uk-push-2-5 uk-grid-width-small-1-1 uk-width-medium-3-5  uk-width-large-3-5">
                                <div class="uk-text-center text-shadow">If anyone destroys God’s temple, God will destroy that person. For God’s temple is holy, and you are that temple.</div>
                                <div class="tm-text-center">
                                    <a href="#" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
                                    <h3 class="uk-panel-header uk-text-center">
                                        <a href="#">1Cor 3:17</a>
                                    </h3>
                                    <a href="#" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1 uk-width-large-1-2">
            <div style="min-height: 210px;" class="uk-panel wptt_TwitterTweets">
                <ul style="height: 172px; overflow: hidden;" class="fetched_tweets">
                    <li style="margin-top: 0px;" class="tweets_avatar">
                        <div class="tweet_wrap">
                            <div class="wdtf-user-card ltr">
                                <div class="clear"></div>
                            </div>
                            <div class="tweet_data">
			        			RT
                                <a href="https://twitter.com/Scripture_Truth" target="_blank" rel="nofollow">@Scripture_Truth</a>: Consider it pure joy, whenever u face trials of many kinds, because u know that the testing of your faith develops perseverance. -Jame 1:2
                            </div>
                            <br>
                            <div class="clear"></div>
                            <div class="times">
                                <em>
                                    <a href="https://www.twitter.com/SET_g47" target="_blank" title="Follow SET on Twitter [Opens new window]">4 months ago</a>
                                </em>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li style="margin-top: 0px;" class="tweets_avatar">
                        <div class="tweet_wrap">
                            <div class="wdtf-user-card ltr">
                                <div class="clear"></div>
                            </div>
                            <div class="tweet_data">
		        				RT
                                <a href="https://twitter.com/BibleIsGodsWord" target="_blank" rel="nofollow">@BibleIsGodsWord</a>: Luke 21:33 - Heaven and earth will pass away, but my words will not pass away.
                            </div>
                            <br>
                            <div class="clear"></div>
                        	<div class="times">
                                <em>
                                    <a href="https://www.twitter.com/SET_g47" target="_blank" title="Follow SET on Twitter [Opens new window]">4 months ago</a>
                                </em>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                    <li style="margin-top: 0px;" class="tweets_avatar">
                        <div class="tweet_wrap">
                            <div class="wdtf-user-card ltr">
                                <div class="clear"></div>
                            </div>
                            <div class="tweet_data">
	        					RT
                                <a href="https://twitter.com/BibleIsGodsWord" target="_blank" rel="nofollow">@BibleIsGodsWord</a>: Matt. 8:8 - “Lord, I am not worthy to have you enter under my roof;
only say the word and my servant will be healed."
                                <a href="https://t.co/uqfknirNPw" target="_blank" rel="nofollow">https://t.co/uqfknirNPw</a>
                            </div>
                            <br>
                            <div class="clear"></div>
                            <div class="times">
                                <em>
                                    <a href="https://www.twitter.com/SET_g47" target="_blank" title="Follow SET on Twitter [Opens new window]">4 months ago</a>
                                </em>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </li>
                </ul>

            </div>
        </div>
    </section>
</div>

<div class="tm-top-e-box tm-light-bg">
    <div class="uk-container uk-container-center">
        <section class="tm-top-e uk-grid tm-xlarge-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <div class="uk-panel CuteLastDonation">
                    <div class="tm-widget-top-text">
                        <h1 class="tm-title tcenter">
                            <span class="border">Charity</span>
                        </h1>
						In sollicitudin, ipsum nec auctor malesuada, elit turpis fermentum odio, ac vestibulum dui risus non elit. Quisque varius ex arcu, vitae rhoncus justo lacinia ac. Nunc eu bibendum mauris, et tincidunt ex. Duis non aliquam nibh.
                    </div>
                    <div class="uk-panel">
                        <div class="uk-width-1-1">
                            <div class="uk-grid uk-grid-collapse">
                                <div class="uk-grid-width-small-1-1 uk-width-medium-1-2 tm-widget-lastdonations-image">
                                    <img src="<?=asset_url();?>images/gall-img-011.jpg" class="attachment-full wp-post-image" alt="gall-img-011" height="1570" width="2000">
                                </div>
                                <div class="uk-grid-width-small-1-1 uk-width-medium-1-2 tm-default-bg">
                                    <div class="tm-text-center">
                                        <a href="donations.html" class="inline-block tm-p-top-medium"><h2>Donation form 5</h2></a>
                                        <div class="yashare-auto-init" data-yasharel10n="ru" data-yasharetype="none" data-yasharequickservices="facebook,twitter,gplus">
                                            <span class="b-share">
                                                <a rel="nofollow" target="_blank" title="Facebook" class="b-share__handle b-share__link b-share-btn__facebook" href="#" data-service="facebook">
                                                    <span class="b-share-icon b-share-icon_facebook"></span>
                                                </a>
                                                <a rel="nofollow" target="_blank" title="Twitter" class="b-share__handle b-share__link b-share-btn__twitter" href="#" data-service="twitter">
                                                    <span class="b-share-icon b-share-icon_twitter"></span>
                                                </a>
                                                <a rel="nofollow" target="_blank" title="Google Plus" class="b-share__handle b-share__link b-share-btn__gplus" href="#" data-service="gplus">
                                                    <span class="b-share-icon b-share-icon_gplus"></span>
                                                </a>
                                            </span>
                                        </div>
                                        <div id="give-form-508-wrap" class="give-form-wrap give-display-modal">
                                            <div class="goal-progress">
                                                <div class="raised">
                                                    <span class="income">$125,906.00</span> of
                                                    <span class="goal-text">$200,000.00 raised</span>
                                                </div>
                                                <div class="progress-bar">
                                                    <span style="width: 62.95%;background-color:#dd9933"></span>
                                                </div-->
                                                <!-- /.progress-bar -->
                                            <!--</div>-->
                                            <!-- /.goal-progress -->
                                           <!-- <div id="give-form-content-508" class="give-form-content-wrap">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean non pulvinar purus. Nunc elit metus, sollicitudin sed nunc sed, iaculis eleifend turpis. Fusce id placerat massa, vitae venenatis mi. Pellentesque sem eros, elementum et mauris sit amet, eleifend vehicula nisi. Nam non arcu dolor.</p>
                                            </div>
                                            <form id="give-form-508" class="give-form give-form_508" method="post">
                                                <input name="give-form-id" value="508" type="hidden">
                                                <input name="give-form-title" value="Donation form 5" type="hidden">
                                                <input name="give-current-url" value="" type="hidden">
                                                <input name="give-form-url" value="" type="hidden">
                                                <div class="give-total-wrap">
                                                   <div class="give-donation-amount form-row-wide">
                                                       <span class="give-currency-symbol give-currency-position-before">$</span>
                                                       <input class="give-text-input" name="give-amount" placeholder="" value="0.00" required autocomplete="off" type="tel">
                                                       <p class="give-loading-text give-updating-price-loader" style="display: none;">
                                                        <span class="give-loading-animation"></span> Updating Amount
                                                        <span class="elipsis">.</span>
                                                        <span class="elipsis">.</span>
                                                        <span class="elipsis">.</span>
                                                       </p>
                                                    </div>
                                                </div>
                                                <button type="button" class="give-btn give-btn-modal">Donate Now</button>
                                                -->
                                                <!-- the checkout fields are loaded into this-->
                                           <!-- </form>-->
                                            <!--end #give-form-508-->
                                        <!--
                                        </div>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
               </div>
			</div>
        </section>
    </div>
</div>

<div class="tm-top-f-box tm-light-bg">
    <section class="tm-top-f uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <div class="uk-width-1-1">
            <div class="uk-panel CutePostGallery">
                <div class="uk-slidenav-position" data-uk-slider="">
                    <div class="uk-slider-container">
                        <ul class="uk-slider uk-grid uk-grid-collapse uk-grid-width-small-1-2 uk-grid-width-medium-1-4 uk-grid-width-large-1-6 uk-grid-width-xlarge-1-6">
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-011.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-011.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-014.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-014.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                	<img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-015.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-015.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-0171.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-0171.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-013.jpg" alt="gall-img-013" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-013.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-010.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-010.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-012.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-012.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-016.jpg" alt="gall-img-016" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-016.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-011.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-011.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-014.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-014.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-015.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-015.jpg"></a>
                            </li>
                            <li>
                                <figure class="uk-overlay uk-overlay-hover">
                                    <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-0171.jpg" alt="" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-0171.jpg"></a>
                            </li>
	                        <li>
	                            <figure class="uk-overlay uk-overlay-hover">
	                                <img draggable="false" class="uk-overlay-scale" src="<?=asset_url();?>images/gall-img-013.jpg" alt="gall-img-013" height="1570" width="2000">
                                </figure>
                                <a draggable="false" class="uk-position-cover" data-uk-lightbox="{group:'gallery-group'}" data-lightbox-type="image" href="<?=asset_url();?>images/gall-img-013.jpg"></a>
                            </li>
                        </ul>
                    </div>
                	<a draggable="false" href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slider-item="previous"></a>
                    <a draggable="false" href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slider-item="next"></a>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="tm-top-g-box tm-light-bg">
    <div class="uk-container uk-container-center">
        <section class="tm-top-g uk-grid tm-xlarge-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
            <div class="uk-width-1-1 uk-width-large-1-2">
                <div style="min-height: 662px;" class="uk-panel CuteLatest">
                    <h3 class="uk-panel-title">Our Blogs</h3>
                    <div class="tm-widget-top-text"></div>
                    <div class="tm-wrap uk-grid-collapse">
                        <div class="tm-latest-post-widget clearfix">
                            <div class="post-date">
                                <div class="day">
                                    <span>29</span>
                                    <p>May</p>
                                </div>
                            </div>
                            <div class="post-image">
                                <img src="<?=asset_url();?>images/post-img-001.jpg" class="attachment-post-small-circular-thumbnails wp-post-image" alt="post-img-001" height="100" width="100">
                            </div>
                            <div class="post-text">
                                <h3 class="uk-panel-header">
                                    <a href="blog-single.html">I am going…</a>
                                </h3>
                                <div>In sollicitudin, ipsum nec auctor malesuada, elit turpis fermentum odio, ac vestibulum dui risus non elit.…</div>
                                <div class="grey">0 Comments | <span>Posted by: alexander-khmelnitskiy</span>
                                </div>
                            </div>
                        </div>
                        <div class="tm-latest-post-widget clearfix">
                            <div class="post-date">
                                <div class="day">
                                    <span>29</span>
                                    <p>May</p>
                                </div>
                            </div>
                            <div class="post-image">
                                <img src="<?=asset_url();?>images/post-img-002.jpg" class="attachment-post-small-circular-thumbnails wp-post-image" alt="post-img-002" height="100" width="100">
                            </div>
                            <div class="post-text">
                                <h3 class="uk-panel-header">
                                    <a href="blog-single.html">Child Dedications</a>
                                </h3>
                                <div>Etiam risus mauris, fermentum sit amet metus ut, vulputate ullamcorper est. Aenean ac turpis dui. Sed…</div>
                                <div class="grey">0 Comments | <span>Posted by: alexander-khmelnitskiy</span>
                                </div>
                            </div>
                        </div>
                        <div class="tm-latest-post-widget clearfix">
                            <div class="post-date">
                                <div class="day">
                                    <span>29</span>
                                    <p>May</p>
                                </div>
                            </div>
                            <div class="post-image">
                                <img src="<?=asset_url();?>images/post-img-003.jpg" class="attachment-post-small-circular-thumbnails wp-post-image" alt="post-img-003" height="100" width="100">
                            </div>
                            <div class="post-text">
                                <h3 class="uk-panel-header">
                                    <a href="blog-single.html">Class: Biblical Vision…</a>
                                </h3>
                                <div>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc condimentum rhoncus…</div>
                                <div class="grey">0 Comments | <span>Posted by: alexander-khmelnitskiy</span>
                                </div>
                            </div>
                        </div>
                        <div class="tm-latest-post-widget clearfix">
                            <div class="post-date">
                                <div class="day">
                                    <span>29</span>
                                    <p>May</p>
                                </div>
                            </div>
                            <div class="post-image">
                                <img src="<?=asset_url();?>images/post-img-004.jpg" class="attachment-post-small-circular-thumbnails wp-post-image" alt="post-img-004" height="100" width="100">
                            </div>
                            <div class="post-text">
                                <h3 class="uk-panel-header">
                                    <a href="blog-single.html">All Church Hymn…</a>
                                </h3>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tempus rhoncus feugiat. Vivamus non aliquam enim,…</div>
                                <div class="grey">0 Comments | <span>Posted by: alexander-khmelnitskiy</span>
                                </div>
                            </div>
                        </div>
                        <div class="tm-latest-post-widget clearfix">
                            <div class="post-date">
                                <div class="day">
                                    <span>17</span>
                                    <p>Jun</p>
                                </div>
                            </div>
                            <div class="post-image">
                                <img src="<?=asset_url();?>images/post-img-005.jpg" class="attachment-post-small-circular-thumbnails wp-post-image" alt="gall-img-016" height="100" width="100">
                            </div>
                            <div class="post-text">
                                <h3 class="uk-panel-header">
                                    <a href="blog-single.html">Aliquam egestas ullamcorper…</a>
                                </h3>
                                <div>Fusce vitae hendrerit eros. Quisque sodales consequat mi eleifend hendrerit. Cras viverra porta rhoncus. Aliquam luctus…</div>
                                <div class="grey">3 Comments | <span>Posted by: alexander-khmelnitskiy</span>
                                </div>
                            </div>
                        </div>
                        <a href="blog.html" title="Blog" class="tm-button-left">Read more</a>
                    </div>
                </div>
            </div>
            <div class="uk-width-1-1 uk-width-large-1-2">
                <div style="min-height: 662px;" class="uk-panel CuteFaq">
                    <h3 class="uk-panel-title">FAQ</h3>
                    <p></p>
                    <div class="uk-grid uk-accordion tm-accordion" data-uk-accordion="">
                        <div class="uk-width-1-1">
                            <h3 class="uk-panel-header uk-accordion-title tm-accordion-title uk-active">Where Can I Find my Purchase Code?</h3>
                            <div aria-expanded="true" role="complementary" data-wrapper="true" style="overflow:hidden;height:0;position:relative;">
                                <div class="uk-accordion-content">Nam in ultricies quam. Integer ut semper augue. Duis ut hendrerit tortor. Nunc pretium magna risus, ut dictum augue ornare in. Nulla vitae faucibus risus. Curabitur nec tortor porttitor, mollis…
                                <a href="#" class="read-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1">
                            <h3 class="uk-panel-header uk-accordion-title tm-accordion-title">Getting Support For Your Item</h3>
                            <div aria-expanded="false" role="complementary" data-wrapper="true" style="overflow:hidden;height:0;position:relative;">
                                <div class="uk-accordion-content">Integer ut semper augue. Duis ut hendrerit tortor. Nunc pretium magna risus, ut dictum augue ornare in. Nulla vitae faucibus risus. Curabitur nec tortor porttitor, mollis purus sodales, fermentum enim.…
                                <a href="#" class="read-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1">
                            <h3 class="uk-panel-header uk-accordion-title tm-accordion-title">Can I Get A Refund?</h3>
                            <div aria-expanded="false" role="complementary" data-wrapper="true" style="overflow:hidden;height:0;position:relative;">
                                <div class="uk-accordion-content">Praesent ullamcorper ornare pharetra. Vestibulum est ipsum, condimentum non sollicitudin sollicitudin, lobortis nec urna. Nulla condimentum sem nec felis vehicula, nec ultricies orci suscipit. Nunc auctor rhoncus volutpat. Class aptent…
                                    <a href="#" class="read-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1">
                            <h3 class="uk-panel-header uk-accordion-title tm-accordion-title">Which Author Payment Option Do I Choose?</h3>
                            <div aria-expanded="false" role="complementary" data-wrapper="true" style="overflow:hidden;height:0;position:relative;">
                                <div class="uk-accordion-content">Sed dictum urna quis tortor auctor, vel auctor arcu mollis. Nulla aliquet sollicitudin ipsum, nec laoreet metus rhoncus eget. Phasellus vitae imperdiet ligula. Donec nec nunc euismod odio imperdiet ornare.…
                                <a href="#" class="read-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1">
                            <h3 class="uk-panel-header uk-accordion-title tm-accordion-title">Envato Market and Intellectual Property</h3>
                            <div aria-expanded="false" role="complementary" data-wrapper="true" style="overflow:hidden;height:0;position:relative;">
                                <div class="uk-accordion-content">Etiam interdum enim risus, a dictum lacus eleifend nec. Donec dui nisi, faucibus nec eros vitae, posuere vehicula erat. Proin id posuere metus. Nunc quam lectus, laoreet id venenatis at,…
                                <a href="#" class="read-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1">
                            <h3 class="uk-panel-header uk-accordion-title tm-accordion-title">Envato Market Content Policy</h3>
                            <div aria-expanded="false" role="complementary" data-wrapper="true" style="overflow:hidden;height:0;position:relative;">
                                <div class="uk-accordion-content">Morbi dignissim augue et elit venenatis, a consequat neque faucibus. Integer cursus mauris vitae dolor tincidunt pulvinar. Fusce sagittis scelerisque scelerisque. Praesent condimentum sapien et dui sagittis, ac dapibus augue…
                                <a href="#" class="read-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1">
                            <h3 class="uk-panel-header uk-accordion-title tm-accordion-title">Was our Help Center useful?</h3>
                            <div aria-expanded="false" role="complementary" data-wrapper="true" style="overflow:hidden;height:0;position:relative;">
                                <div class="uk-accordion-content">Donec nec nunc euismod odio imperdiet ornare. Maecenas quis ipsum rhoncus, ultrices ligula eu, interdum dui. In sit amet volutpat erat. In nunc nisl, varius a accumsan quis, imperdiet sit…
                                <a href="#" class="read-more">read more</a>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-1-1">
                            <h3 class="uk-panel-header uk-accordion-title tm-accordion-title">How to edit form.</h3>
                            <div aria-expanded="false" role="complementary" data-wrapper="true" style="overflow:hidden;height:0;position:relative;">
                                <div class="uk-accordion-content">Donec nec nunc euismod odio imperdiet ornare. Maecenas quis ipsum rhoncus, ultrices ligula eu, interdum dui. In sit amet volutpat erat. In nunc nisl, varius a accumsan quis, imperdiet sit…
                                <a href="#" class="read-more">read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="uk-container uk-container-center"></div>

<div class="tm-bottom-a-box tm-light-bg">
    <section class="tm-bottom-a uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
        <div class="uk-width-1-1">
            <div class="uk-panel CuteLocation">
                <div class="">
                    <div class="uk-panel tm-location-widget" data-uk-scrollspy="{cls:'uk-animation-slide-right', repeat: true}">
                        <div class="uk-grid tm-location-box uk-grid-collapse">
                            <div class="uk-width-small-1-1 uk-width-medium-1-2  tm-location-infoblock">
                                <h3 class="tm-location-title">
                                    <a href="#">Bethlehem Church</a>
                                </h3>
                                <div class="tm-location-info">
                                    <p>
                                        <i class="uk-icon-map-marker"></i>1800 Grand Ave, San Diego, CA 92109
                                    </p>
                                    <p>
                                        <i class="uk-icon-phone"></i>(619) 555-9740
                                    </p>
                                    <p>
                                        <i class="uk-icon-envelope">&nbsp;</i>
                                        <a href="mailto:cutechurch@church.com">cutechurch@church.com</a>
                                    </p>
                                </div>
                            </div>
                            <div class="uk-width-small-1-1 uk-width-medium-1-2 ">
                                <a href="#" class="">
                                    <img src="<?=asset_url();?>images/gall-img-016.jpg" class="attachment-full wp-post-image" alt="gall-img-016" height="1570" width="2000">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="uk-panel tm-location-widget" data-uk-scrollspy="{cls:'uk-animation-slide-left', repeat: true}">
                        <div class="uk-grid tm-location-box uk-grid-collapse">
                            <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-push-1-2  tm-location-infoblock">
                                <h3 class="tm-location-title">
                                    <a href="#">Church of Jesus Christ</a>
                                </h3>
                                <div class="tm-location-info">
                                    <p>
                                        <i class="uk-icon-map-marker"></i>1800 Grand Ave, San Diego, CA 92109
                                    </p>
                                    <p>
                                        <i class="uk-icon-phone"></i>(619) 555-9740
                                    </p>
                                    <p>
                                        <i class="uk-icon-envelope">&nbsp;</i>
                                        <a href="mailto:cutechurch@church.com">cutechurch@church.com</a>
                                    </p>
                                </div>
                            </div>
                            <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-pull-1-2 ">
                                <a href="#" class="">
                                    <img src="<?=asset_url();?>images/gall-img-0022.jpg" class="attachment-full wp-post-image" alt="gall-img-002" height="1340" width="2000">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="uk-panel tm-location-widget" data-uk-scrollspy="{cls:'uk-animation-slide-right', repeat: true}">
                        <div class="uk-grid tm-location-box uk-grid-collapse">
                            <div class="uk-width-small-1-1 uk-width-medium-1-2  tm-location-infoblock">
                                <h3 class="tm-location-title">
                                    <a href="#">Church of la Santísima Trinidad</a>
                                </h3>
                                <div class="tm-location-info">
                                    <p>
                                        <i class="uk-icon-map-marker"></i>1800 Grand Ave, San Diego, CA 92109
                                    </p>
                                    <p>
                                        <i class="uk-icon-phone"></i>(619) 555-9740
                                    </p>
                                    <p>
                                        <i class="uk-icon-envelope">&nbsp;</i>
                                        <a href="mailto:cutechurch@church.com">cutechurch@church.com</a>
                                    </p>
                                </div>
                            </div>
                            <div class="uk-width-small-1-1 uk-width-medium-1-2 ">
                                <a href="#" class="">
                                    <img src="<?=asset_url();?>images/gall-img-013.jpg" class="attachment-full wp-post-image" alt="gall-img-013" height="1570" width="2000">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="uk-panel tm-location-widget" data-uk-scrollspy="{cls:'uk-animation-slide-left', repeat: true}">
                        <div class="uk-grid tm-location-box uk-grid-collapse">
                            <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-push-1-2  tm-location-infoblock">
                                <h3 class="tm-location-title">
                                    <a href="#">St Germain's Church</a>
                                </h3>
                                <div class="tm-location-info">
                                    <p>
                                        <i class="uk-icon-map-marker"></i>1800 Grand Ave, San Diego, CA 92109
                                    </p>
                                    <p>
                                        <i class="uk-icon-phone"></i>(619) 555-9740
                                    </p>
                                    <p>
                                        <i class="uk-icon-envelope">&nbsp;</i>
                                        <a href="mailto:cutechurch@church.com">cutechurch@church.com</a>
                                    </p>
                                </div>
                            </div>
                            <div class="uk-width-small-1-1 uk-width-medium-1-2 uk-pull-1-2 ">
                                <a href="#" class="">
                                    <img src="<?=asset_url();?>images/gall-img-0012.jpg" class="attachment-full wp-post-image" alt="gall-img-001" height="1340" width="2000">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="tm-bottom-c-box tm-light-bg">
    <div class="uk-container uk-container-center">
        <section class="tm-bottom-c uk-grid tm-large-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
            <div class="uk-width-1-1">
                <div class="uk-panel CuteTestimonials">
                    <div class="tm-widget-top-text">
                        <h1 class="tm-title tcenter">
                            <span class="border">Testimonials</span>
                        </h1>
						In sollicitudin, ipsum nec auctor malesuada, elit turpis fermentum odio, ac vestibulum dui risus non elit. Quisque varius ex arcu, vitae rhoncus justo lacinia ac. Nunc eu bibendum mauris, et tincidunt ex. Duis non aliquam nibh.
                    </div>
                    <div data-uk-slideset="{small: 1, medium: 2, large: 2}" class="tm-slideset">
                        <div class="uk-slidenav-position">
                            <ul class="uk-grid uk-slideset uk-grid-width-1-1 uk-grid-width-large-1-2 uk-grid-width-medium-1-2 uk-grid-width-small-1-1">
                                <li class="uk-active" style="">
                                    <div class="uk-panel">
                                        <p>
                                            <span class="circle-border">
                                                <img src="<?=asset_url();?>images/img_testim_001.jpg" class="attachment-thumbnail wp-post-image" alt="img_testim_001" height="164" width="164">
                                            </span>
                                        </p>
                                        <h3 class="uk-panel-header">
                                            <span class="brackets">
                                                <a href="our-pastors.html">Don Nelson</a>
                                            </span>
                                        </h3>
                                        <div class="tm-text-center">Nunc finibus laoreet elit. Suspendisse scelerisque, lorem at lacinia tempus, magna augue pulvinar massa, at…</div>
                                    </div>
                                </li>
                                <li class="uk-active" style="">
                                    <div class="uk-panel">
                                        <p>
                                            <span class="circle-border">
                                                <img src="<?=asset_url();?>images/img_pastors_002.jpg" class="attachment-thumbnail wp-post-image" alt="img_pastors_002" height="164" width="164">
                                            </span>
                                        </p>
                                        <h3 class="uk-panel-header">
                                            <span class="brackets">
                                                <a href="our-pastors.html">Rafael Stone</a>
                                            </span>
                                        </h3>
                                        <div class="tm-text-center">Nulla elementum interdum pulvinar. Donec tempor dapibus ipsum, eu pulvinar est consectetur et. Mauris interdum…</div>
                                    </div>
                                </li>
                                <li style="display: none;">
                                    <div class="uk-panel">
                                        <p>
                                            <span class="circle-border">
                                                <img src="<?=asset_url();?>images/img_testim_004.jpg" class="attachment-thumbnail wp-post-image" alt="img_testim_004" height="164" width="164">
                                            </span>
                                        </p>
                                        <h3 class="uk-panel-header">
                                            <span class="brackets">
                                                <a href="our-pastors.html">Francisco Caldwell</a>
                                            </span>
                                        </h3>
                                        <div class="tm-text-center">Nulla elementum interdum pulvinar. Donec tempor dapibus ipsum, eu pulvinar est consectetur et. Mauris interdum…</div>
                                    </div>
                                </li>
                                <li style="display: none;">
                                    <div class="uk-panel">
                                        <p>
                                            <span class="circle-border">
                                                <img src="<?=asset_url();?>images/img_testim_002.jpg" class="attachment-thumbnail wp-post-image" alt="img_testim_002" height="164" width="164">
                                            </span>
                                        </p>
                                        <h3 class="uk-panel-header">
                                            <span class="brackets">
                                                <a href="our-pastors.html">Albert Lane</a>
                                            </span>
                                        </h3>
                                        <div class="tm-text-center">In hac habitasse platea dictumst. Aliquam ullamcorper nunc nec pretium faucibus. Aenean semper lorem nisl,…</div>
                                    </div>
                                </li>
                                <li style="display: none;">
                                    <div class="uk-panel">
                                        <p>
                                            <span class="circle-border">
                                                <img src="<?=asset_url();?>images/img_pastors_005.jpg" class="attachment-thumbnail wp-post-image" alt="img_pastors_005" height="164" width="164">
                                            </span>
                                        </p>
                                        <h3 class="uk-panel-header">
                                            <span class="brackets">
                                                <a href="our-pastors.html">Hazel Berry</a>
                                            </span>
                                        </h3>
                                        <div class="tm-text-center">Aenean quis porta nisi. Duis ultricies lorem ut nisi vehicula, id malesuada ex facilisis. Donec…</div>
                                    </div>
                                </li>
                                <li style="display: none;">
                                    <div class="uk-panel">
                                        <p>
                                            <span class="circle-border">
                                                <img src="<?=asset_url();?>images/img_testim_003.jpg" class="attachment-thumbnail wp-post-image" alt="img_testim_003" height="164" width="164">
                                            </span>
                                        </p>
                                        <h3 class="uk-panel-header">
                                            <span class="brackets">
                                                <a href="our-pastors.html">Bessie Curtis</a>
                                            </span>
                                        </h3>
                                        <div class="tm-text-center">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin sodales,…</div>
                                    </div>
                                </li>
                                <li style="display: none;">
                                    <div class="uk-panel">
                                        <p>
                                            <span class="circle-border">
                                                <img src="<?=asset_url();?>images/img_testim_005.jpg" class="attachment-thumbnail wp-post-image" alt="img_testim_005" height="164" width="164">
                                            </span>
                                        </p>
                                        <h3 class="uk-panel-header">
                                            <span class="brackets">
                                                <a href="our-pastors.html">John Doe</a>
                                            </span>
                                        </h3>
                                        <div class="tm-text-center">Aenean dignissim accumsan dolor sed porttitor. Ut mollis ipsum dolor, quis maximus dolor vehicula sit…</div>
                                    </div>
                                </li>
                                <li style="display: none;">
                                    <div class="uk-panel">
                                        <p>
                                            <span class="circle-border">
                                                <img src="<?=asset_url();?>images/img_testim_006.jpg" class="attachment-thumbnail wp-post-image" alt="img_testim_006" height="164" width="164">
                                            </span>
                                        </p>
                                        <h3 class="uk-panel-header">
                                            <span class="brackets">
                                                <a href="our-pastors.html">Jack Daniels</a>
                                            </span>
                                        </h3>
                                        <div class="tm-text-center">Nunc finibus laoreet elit. Suspendisse scelerisque, lorem at lacinia tempus, magna augue pulvinar massa, at…</div>
                                    </div>
                                </li>
                            </ul>
                            <a href="" data-uk-slideset-item="previous" class="tm-slideset-previous"></a>
                            <a href="" data-uk-slideset-item="next" class="tm-slideset-next"></a>
                        </div>
                        <a href="testimonials.html" title="Testimonials" class="tm-button-center">All testimonials</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="tm-bottom-d-box tm-dark-bg">
    <section class="tm-bottom-d uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
        <div class="uk-width-1-1">
            <div class="uk-panel CutePartners" >
                <div data-uk-slider="{center:true}" >
                    <div class="uk-slider-container">
                        <div class="uk-slider">
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-002_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-002_01" />
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-003_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-003_01" />
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-001_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-001_01" />
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-004_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-004_01" />
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-005_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-005_01" />
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-006_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-006_01" />
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-007_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-007_01" />
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-008_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-008_01" />
                            <img width="227" height="160" src="<?=asset_url();?>images/partner-009_01.png" class="attachment-post-thumbnail wp-post-image" alt="partner-009_01" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="tm-bottom-e-box tm-light-bg">
    <section class="tm-bottom-e uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
        <div class="uk-width-1-1">
            <div class="uk-panel widget_text" >
	            <div class="map-wrap">
	            	<script>
	                    window.map = false;
	                    window.marker = '<?=asset_url();?>images/map-marker.png';

	                    function create_marker(marker_data){
	                        var infowindow = new google.maps.InfoWindow({
	                            content: '<div class="map-marker-content">'+
	                                    '<h3>'+marker_data.header+'</h3>'+
	                                    '<p class="address">'+marker_data.address+'</p>'+
	                                    '<p class="address_2">'+marker_data.address_2+'</p>'+
	                                '</div>'
	                        });
	                        var marker = new google.maps.Marker({
	                            position: new google.maps.LatLng(marker_data.lat,marker_data.lng),
	                            map: window.map,
	                            title: marker_data.header,
	                            icon:window.marker
	                        });
	                        google.maps.event.addListener(marker, 'click', function() {
	                            infowindow.open(window.map,marker);
	                        });
	                    }

	                    function initialize(){
	                        var myLatlng = new google.maps.LatLng(50.3915097,-4.1306689);

	                        var mapOptions = {
	                            zoom:16,
	                            center: myLatlng,
	                            mapTypeId: google.maps.MapTypeId.ROADMAP,
	                            scrollwheel: false,

	                            streetViewControl:false,
	                            overviewMapControl:false,
	                            mapTypeControl:false

	                        };

	                        window.map = new google.maps.Map(document.getElementById('map'), mapOptions);																																																														 						create_marker({
	                            header:'Cute Curch',
	                            address:'Mannamead Road, Plymouth PL3 5QJ',
	                            address_2:'another info',

	                            lat:50.3915097,
	                            lng:-4.1306689
	                        });

	                    }

	                    google.maps.event.addDomListener(window, 'load', initialize);
            </script>
	                <div id="map"></div>
	            </div>
            </div>
        </div>
    </section>
</div>

<div class="tm-bottom-f-box tm-default-bg">
    <div class="uk-container uk-container-center">
        <section class="tm-bottom-f uk-grid tm-xlarge-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
            <div class="uk-width-1-1">
                <div class="uk-panel widget_text" >
                    <div class="uk-grid get-it-now-box">
                        <div class="uk-width-large-4-5 uk-width-medium-4-5 uk-width-small-1-1 text-widget-block">
                            <span>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet</span>
                            <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</p>
                        </div>
                        <div class="uk-width-large-1-5 uk-width-medium-1-5 uk-width-small-1-1">
                            <a class="tm-button-left" href="/">get it now</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>-->
