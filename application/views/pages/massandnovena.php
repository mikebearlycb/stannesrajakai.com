<div class="tm-slider-box tm-light-bg">
    <section class="tm-slider uk-grid tm-none-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
        <!-- start full width -->
        <div class="uk-width-1-1">
            <div class="uk-panel top-pageslider CuteChurch">
                <div class="slider-module">
                    <div class="uk-slidenav-position" data-uk-slideshow="{height: &#039;300&#039;, animation: &#039;fade&#039;, duration: &#039;&#039;, autoplay: true, autoplayInterval: &#039;5000&#039;, videoautoplay: false, videomute: false, kenburns: false}">
                        <ul class="uk-slideshow uk-overlay-active">
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page.jpg" alt="bg_page" width="1920" height="360" class="aligncenter size-full" />
                            </li>
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page-01.jpg" alt="bg_page-01" width="1920" height="300" class="aligncenter size-full" />
                            </li>
                            <li class="uk-cover uk-height-viewport  tm-wrap"><img src="<?=asset_url();?>/images/bg_page-02.jpg" alt="bg_page-02" width="1920" height="300" class="aligncenter size-full" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- end full width -->
    </section>
</div>

<div class="uk-container uk-container-center">
    <div class="uk-grid" data-uk-grid-match data-uk-grid-margin>
        <div class="tm-main uk-width-medium-1-1 tm-middle">
            <main class="tm-content">
                <section>

                    <div id="primary" class="site-content post-content">
                        <div class="main-heading">
                            <h1 class="tm-page-title">பங்கின் வழிபாடு நிகழ்வுகள்</h1>
                        </div>
                        <div id="content" role="main">
                          <div>
                        <img src="<?=asset_url();?>images/mass_timings.png"/>
                    </div>

                          <h2 class="tribe-events-single-event-title summary entry-title">Mass timings and events</h2>
                            <h3>Sunday Mass Timings</h3>
                            <p>Early Morning 5.00 am First Mass</p>
                            <p>Morning 7.00 am Second Mass</p>
                            <p>Evening 7.00 pm Adoration</p>

                            <h3>Weekdays</h3>
                            <p>Early Morning 5:30 am Mass</p>
                            <p>Every tuesdays 5:30pm Mass at St. Antony's Church, Parapuvilai</p>

                            <h3>First Tuesday of Every Month</h3>
                            <p>Evening 5.00 pm Mass and Novena with possesion at St Lourd's Grotto, Parapuvilai.</p>
                            <p>There is no Mass at the Parish Church.</p>

                            <h3>Fourth Wednesday of Every Month</h3>
                            <p>Our Lady of Good Health Litany and Novena Mass at 7.00 pm<p>

                            <h3>Every Thursdays</h3>
                            <p>Evening 7.00 pm St. Anne's Litany and Novena Mass</p>
                            <p>Morning 5:30 am Mass on every third Thrusday's</p>
                            <p>Evening 7.00 pm Adoration</p>

                            <h3>Every Saturday</h3>
                            <p>Evening St Lourd's Litany and Novena Mass at 7.00 pm</p>

                            <h3>4th Saturday of Every Month</h3>
                            <p>Mass at evening 5:30 pm</p>
                            <p>Evening 6:30 pm Mass at St Michael's Grotto</p>
   <!-- Notices -->
                            <h2 class="tribe-events-single-event-title summary entry-title">Significant Events of Parish</h2>
                            <br/>
                            <h3>St Anne's Church, Rajkai Feast</h3>
                            <p>Starts on 17th of July with Falg hoisting and ends on 26th of July</p>

                            <h3>St Antony's Church, Parappuviali Feast</h3>
                            <p>Tuesday before Ash Wednesday</p>

                            <h3>Rajakai's Asanam</h3>
                            <p>First Tuesday of December</p>



                        </div>
                        <!-- #content -->
                    </div>
                    <!-- #primary -->
                </section>
            </main>
        </div>
    </div>
</div>
