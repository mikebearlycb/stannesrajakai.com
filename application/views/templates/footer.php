
<div class="tm-footer-top-box tm-dark-bg">
    <!--
    <div class="uk-container uk-container-center">
        <section class="tm-footer-top uk-grid tm-medium-padding" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>

            <div class="uk-width-1-1 uk-width-large-1-3">
                <div class="uk-panel widget_text">
                    <h3 class="uk-panel-title">About us</h3>
                    <p>In vitae velit mauris. Donec sodales posuere pretium. Suspendisse dolor tortor, fringilla sed feugiat quis, imperdiet vitae tortor. Suspendisse et magna ligula. Ut nec facilisis lectus. Maecenas venenatis sem aliquam, tempor ante ut, facilisis ... </p>
                </div>
            </div>

            <div class="uk-width-1-1 uk-width-large-1-3">
                <div class="uk-panel widget_text">
                    <h3 class="uk-panel-title">Links</h3>
                    <ul class="footer-links">
                        <li><a href="http://www.facebook.com" target="_blank">In vitae velit mauris</a>
                        </li>
                        <li><a href="http://twitter.com/" target="_blank">Lorem ipsum dolor</a>
                        </li>
                        <li><a href="http://plus.google.com/" target="_blank">Donec vitae tortor</a>
                        </li>
                        <li><a href="http://www.linkedin.com" target="_blank">Donec magna sapien</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="uk-width-1-1 uk-width-large-1-3">
                <div class="uk-panel widget_text">
                    <h3 class="uk-panel-title">Newslatter</h3>
                    <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</p>
                    <div class="subscribe">
                        <div role="form" class="wpcf7" id="wpcf7-f405-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response"></div>
                            <form method="post" class="wpcf7-form" novalidate="novalidate">
                                <div style="display: none;">
                                    <input type="hidden" name="_wpcf7" value="405" />
                                    <input type="hidden" name="_wpcf7_version" value="4.3" />
                                    <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                    <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f405-o1" />
                                    <input type="hidden" name="_wpnonce" value="7eb6dd6d20" />
                                </div>
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-grid-width-small-4-6 uk-width-medium-4-6 uk-grid-width-large-4-6">
                                        <span class="wpcf7-form-control-wrap email-639"><input type="email" name="email-639" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Your email" /></span>
                                    </div>
                                    <div class="uk-grid-width-small-2-6 uk-width-medium-2-6 uk-grid-width-large-2-6">
                                        <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" />
                                    </div>
                                </div>
                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    -->
    <!-- end full width -->
</div>

<footer class="tm-footer-box tm-none-bg tm-none-padding">

    <div class="uk-container uk-container-center">
        <div class="tm-footer uk-grid">
            <div class="uk-width-1-1 uk-width-large-1-2">
                <div class="uk-panel tm-p-medium  widget_text">
                    <div class="tm-text-left">
                        © 2020 St Annes Church - Rajakai All rights reserved.
                    </div>
                </div>
            </div>

            <div class="uk-hidden-small uk-hidden-medium uk-width-large-1-2">
                <div class="uk-panel uk-hidden-medium uk-hidden-small tm-p-medium  widget_text">
                    <div class="tm-text-right">
                        <ul class="social-icons">
                            <li><a href="https://www.facebook.com/rajakrishnapura/" target="_blank"><i class="uk-icon-facebook-f"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/rajakrishnapura/" target="_blank"><i class="uk-icon-twitter"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/rajakrishnapura/" target="_blank"><i class="uk-icon-google-plus"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/rajakrishnapura/" target="_blank"><i class="uk-icon-linkedin"></i></a>
                            </li>
                            <li><a href="https://www.youtube.com/channel/UCxV4u6RRTZ0SLqOx-1xSoZQ" target="_blank"><i class="uk-icon-youtube"></i></a>
                            </li>
                            <li><a href="http://vk.com/" target="_blank"><i class="uk-icon-vk"></i></a>
                            </li>
                            <li><a href="https://www.facebook.com/rajakrishnapura/" target="_blank"><i class="uk-icon-instagram"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end full width -->
</footer>

<script type="text/javascript" src="<?=asset_url();?>js/jquery.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/uikit.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/slider.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/slideset.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/slideshow.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/slideshow-fx.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/sticky.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/lightbox.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/accordion.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/countdown.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/grid.js"></script>
<script type="text/javascript" src="<?=asset_url();?>js/components/sortable.js"></script>
</body>
</html>
