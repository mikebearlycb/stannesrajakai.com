<!DOCTYPE html>
<html lang="ru">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
<title>புனித&nbsp;அன்னம்மாள்&nbsp;ஆலயம்,&nbsp;இரஜகை</title>

<link href="<?=asset_url();?>css/theme.css" rel="stylesheet" type="text/css" />
<link href="<?=asset_url();?>css/woocommerce.css" rel="stylesheet" type="text/css" />
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>

<header>
	 <div class="tm-header-offset">  <!-- Start header offset-->
		<div class="tm-head-a-box tm-medium-padding">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<!-- Start adress widget area -->
					<div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-3 tm-text-left">
						<img width="65" height="70" src="<?=asset_url();?>images/greatlogo.png"/>
					</div>
					<!-- End adress widget area -->
					<!-- Start top-logo widget area -->
					<div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-3 uk-text-center" style="font-size: 25px">
						<br/>
					    புனித&nbsp;அன்னம்மாள்&nbsp;ஆலயம்,&nbsp;இரஜகை
					    	<br/>
					    	<br/>
					    	St. Annes Church, Rajakai
					</div>
					<!-- End top-logo widget area -->
					<!-- Start social-icons & search widget area -->
					<div class="uk-width-small-1-1 uk-width-medium-1-3 uk-width-large-1-3 tm-text-right">
						<div class="social-icons clearfix">
							<div class=" uk-hidden-small tm-p-top-medium widget_text">
								04652-267393 <br/> இரஜகை - 627114, திருநெல்வேலி <br/>  தூத்துக்குடி மறைமாவட்டம்
							</div>
						</div>
						<!-- End search widget area -->
						<!-- End search widget area -->
					</div>
					<!-- End social-icons widget area -->
				</div>
			</div>
		</div>

		<!-- End fixed menu-->
		<div class="breadcrumbs">
			<div class="uk-container uk-container-center breadcrumbs-box"></div>
		</div>
	</div>  <!-- End header offset-->
</header>
